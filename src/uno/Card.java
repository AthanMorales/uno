/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

/**
 *
 * @author Athan
 */
public abstract class Card {
    protected EnumColor color;
    protected EnumNumber number;
    protected EnumType type;

    public Card(EnumColor color,EnumType type) {
        this.color = color;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Card{" + "color=" + color + ", number=" + number + ", type=" + type + '}';
    }
}
