/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

import java.util.Random;

/**
 *
 * @author Athan
 */
public class RandomItem {
    
    public static EnumType generateRandomType() {
        EnumType[] values = EnumType.values();
        int length = values.length;
        int randIndex = new Random().nextInt(length);
        return values[randIndex];
    }
    
    public static EnumNumber generateRandomNumber() {
        EnumNumber[] values = EnumNumber.values();
        int length = values.length;
        int randIndex = new Random().nextInt(length);
        return values[randIndex];
    }
    
    public static EnumColor generateRandomColor() {
        EnumColor[] values = EnumColor.values();
        int length = values.length;
        int randIndex = new Random().nextInt(length);
        return values[randIndex];
    }
}
