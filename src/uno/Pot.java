/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

/**
 *
 * @author Athan
 */
public class Pot {
    private Card card;
    
    public String showPot(CardFactory factory) {
        EnumType type;
        Card randomCard;
        do{
            type = RandomItem.generateRandomType();
            randomCard = factory.getCard(type, RandomItem.generateRandomNumber() , RandomItem.generateRandomColor());
        }while(type != EnumType.NUMBER);
        card = randomCard;
        String potData = ("The Card is now in the pot is: " + card);
        return potData;
    }
    
    public void setCard(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }
}
