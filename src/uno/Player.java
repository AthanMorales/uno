/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

/**
 *
 * @author Athan
 */
public class Player {
    private String name;
    private Hand playerHand;
    private int score;
    private boolean turn;
    private int playerNumber;

    public Player(String name) {
        this.name = name;
        this.score = 0;
        this.turn = false;
    }
    
    public String getName() {
        return name;
    }

    public Hand getPlayerHand() {
        return playerHand;
    }

    public void setPlayerHand(Hand playerHand) {
        this.playerHand = playerHand;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }
    
    

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", playerHand=" + playerHand + ", score=" + score + ", turn=" + turn + ", playerNumber=" + playerNumber + '}';
    }
}
