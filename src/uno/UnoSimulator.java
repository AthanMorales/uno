/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Athan
 */
public class UnoSimulator {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Uno uno = new Uno();
        
        System.out.println("Welcome to UNO game");
        System.out.println("Enter the number of players");
        int numberOfPlayers = Integer.parseInt(br.readLine());        
        uno.setNumberOfplayers(numberOfPlayers);
        String name;
        
        for(int i = 0; i < uno.getNumberOfplayers(); i++){
            name = br.readLine();
            uno.registerPlayer(name, i);
        }
        uno.getPlayers().get(0).setTurn(true);
        printAllPlayers(uno);
        uno.dealCards();
        printPlayersHands(uno);
        
        System.out.println(uno.getPot().showPot(uno.getFactory()));
        
//        uno.play();

    }
    
    public static void printAllPlayers(Uno uno){
        for(int i = 0; i < uno.getPlayers().size(); i++){
            System.out.println(uno.getPlayers().get(i));
        }
        System.out.println("");
    }
    
    public static void printPlayersHands(Uno uno){
        for(int i = 0; i < uno.getPlayers().size(); i++){
            System.out.println("Player" + i + " hand");
            uno.printHandPlayer(i);
        }
    }
}
