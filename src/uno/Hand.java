/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

import java.util.ArrayList;

/**
 *
 * @author Athan
 */
public class Hand {
    private ArrayList<Card> hand = new ArrayList<>();
    
    public void addCard(Card card){
        hand.add(card);
    }
    
    public Card getCartd(int selection){
        return hand.get(selection);
    }
    
    public int getHandSize(){
        return hand.size();
    }
    
    public void removeCard(int index){
        hand.remove(index);
    }
}
