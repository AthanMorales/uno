/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

/**
 *
 * @author Athan
 */
public class CardWildDrawFour extends Card {
    public CardWildDrawFour(EnumColor color, EnumType type) {
        super(color, type);
        this.color = EnumColor.BLACK;
    }

    public void setColor(EnumColor color) {
        super.color = color;
    }
}
