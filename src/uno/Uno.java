/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

import java.util.ArrayList;

/**
 *
 * @author Athan
 * 
 */
public class Uno {
    private boolean gameDirection = true;
    private int numberOfplayers;
    private int numberOfGamesPlayed;
    private int globalScore;
    private ArrayList<Player> players;
    private Pot pot;
    private CardFactory factory;

    public Uno() {
        players = new ArrayList<>();
        pot = new Pot();
        factory = CardFactory.getInstance();
    }
    
    public void printHandPlayer(int currentPlayer){
        for(int i = 0; i < players.get(currentPlayer).getPlayerHand().getHandSize(); i++){
            System.out.println(i + "." + players.get(currentPlayer).getPlayerHand().getCartd(i));
        }
        System.out.println("");
    }
    
    public void registerPlayer(String name, int playerNumber){
        Player player = new Player(name);
        player.setPlayerNumber(playerNumber);
        players.add(player);
    }
    
    public void dealCards(){
        for(int i = 0; i < numberOfplayers; i++){
            Hand hand = new Hand();
            for(int j = 0; j < 7; j++ ){
                Card randomCard = factory.getCard(RandomItem.generateRandomType(), RandomItem.generateRandomNumber() , RandomItem.generateRandomColor());
                hand.addCard(randomCard);
            }
            players.get(i).setPlayerHand(hand);
        }
    }


    public boolean isGameDirection() {
        return gameDirection;
    }

    public void setGameDirection(boolean gameDirection) {
        this.gameDirection = gameDirection;
    }

    public int getNumberOfGamesPlayed() {
        return numberOfGamesPlayed;
    }

    public void setNumberOfGamesPlayed(int numberOfGamesPlayed) {
        this.numberOfGamesPlayed = numberOfGamesPlayed;
    }

    public int getGlobalScore() {
        return globalScore;
    }

    public void setGlobalScore(int globalScore) {
        this.globalScore = globalScore;
    }

    public int getNumberOfplayers() {
        return numberOfplayers;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setNumberOfplayers(int numberOfplayers) {
        this.numberOfplayers = numberOfplayers;
    }

    public CardFactory getFactory() {
        return factory;
    }

    public Pot getPot() {
        return pot;
    }
}
