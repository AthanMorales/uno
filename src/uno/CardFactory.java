/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;
/**
 *
 * @author Athan
 */
public class CardFactory {
   
    private static CardFactory cardFactory;

    private CardFactory() {
    }

    public static CardFactory getInstance() {
        if (cardFactory == null) {
            cardFactory = new CardFactory();
        }
        return cardFactory;
    }
    
    public Card getCard(EnumType type, EnumNumber number, EnumColor color) {
        switch(type){
            case NUMBER: return new CardNumber(color, number,type);
            case SKIP: return new CardSkip(color,type);
            case REVERSE: return new CardReverse(color,type);
            case DRAW_TWO: return new CardDrawTwo(color,type);
            case WILD_COLOR: return new CardWildColor(color, type);
            case WILD_DRAW_FOUR: return new CardWildDrawFour(color,type);
        }
        return null;
    }
}