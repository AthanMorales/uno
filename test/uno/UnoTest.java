/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Athan
 */
public class UnoTest {
    
    public UnoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testRegisterPlayer() {
        System.out.println("registerPlayer Regular");
        String name = "";
        int playerNumber = 0;
        Uno instance = new Uno();
        instance.registerPlayer(name, playerNumber);
        boolean expResult = false;
        boolean result = instance.getPlayers().isEmpty();
        assertEquals("No objet was added to the list",expResult, result);
    }
    @Test
    public void testRegisterPlayerExeption() {
        System.out.println("registerPlayer Exeption (no items in list)");
        Uno instance = new Uno();
        boolean expResult = true;
        boolean result = instance.getPlayers().isEmpty();
        assertEquals("No objet was added to the list",expResult, result);
    }


    @Test
    public void testIsGameDirection() {
        System.out.println("isGameDirection");
        Uno instance = new Uno();
        boolean expResult = true;
        boolean result = instance.isGameDirection();
        assertEquals("The returned value does not match",expResult, result);
    }
    

    @Test
    public void testSetGameDirection() {
        System.out.println("setGameDirection");
        boolean gameDirection = false;
        Uno instance = new Uno();
        instance.setGameDirection(gameDirection);
        boolean expResult = false;
        boolean result = instance.isGameDirection();
        assertEquals("The returned value does not match",expResult, result);
    }

    @Test
    public void testGetNumberOfGamesPlayed() {
        System.out.println("getNumberOfGamesPlayed");
        Uno instance = new Uno();
        instance.setNumberOfGamesPlayed(0);
        int expResult = 0;
        int result = instance.getNumberOfGamesPlayed();
        assertEquals("The returned value does not match",expResult, result);
    }

    @Test
    public void testSetNumberOfGamesPlayed() {
        System.out.println("setNumberOfGamesPlayed");
        int numberOfGamesPlayed = 0;
        Uno instance = new Uno();
        instance.setNumberOfGamesPlayed(numberOfGamesPlayed);
        int expResult = 0;
        int result = instance.getNumberOfGamesPlayed();
        assertEquals("The returned value does not match",expResult, result);
    }

    @Test
    public void testGetGlobalScore() {
        System.out.println("getGlobalScore");
        Uno instance = new Uno();
        instance.setGlobalScore(0);
        int expResult = 0;
        int result = instance.getGlobalScore();
        assertEquals("The returned value does not match",expResult, result);
    }

    @Test
    public void testSetGlobalScore() {
        System.out.println("setGlobalScore");
        int globalScore = 0;
        Uno instance = new Uno();
        instance.setGlobalScore(globalScore);
        int expResult = 0;
        int result = instance.getGlobalScore();
        assertEquals("The returned value does not match",expResult, result);
    }

    @Test
    public void testGetNumberOfplayers() {
        System.out.println("getNumberOfplayers");
        Uno instance = new Uno();
        instance.setNumberOfplayers(0);
        int expResult = 0;
        int result = instance.getNumberOfplayers();
        assertEquals("The returned value does not match",expResult, result);
    }

    @Test
    public void testGetPlayers() {
        System.out.println("getPlayers");
        Uno instance = new Uno();
        ArrayList<Player> player = new ArrayList<>(); 
        ArrayList<Player> expResult = player;
        ArrayList<Player> result = instance.getPlayers();
        assertEquals("The returned objet is not an object list",expResult, result);
    }

    @Test
    public void testSetNumberOfplayers() {
        System.out.println("setNumberOfplayers");
        int numberOfplayers = 0;
        Uno instance = new Uno();
        instance.setNumberOfplayers(numberOfplayers);
        int expResult = 0;
        int result = instance.getNumberOfplayers();
        assertEquals("The returned value does not match",expResult, result);
    }

    @Test
    public void testGetFactory() {
        System.out.println("getFactory");
        Uno instance = new Uno();
        CardFactory factory = CardFactory.getInstance();
        CardFactory expResult = factory;
        CardFactory result = instance.getFactory();
        assertEquals("The returned objet is not an object factory",expResult, result);
    }
    
}
